.PHONY: deps rel  quick-test tree dist

APP=chunter

uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
uname_V6 := $(shell sh -c 'uname -v 2>/dev/null | cut -c-6 || echo not')


ifeq ($(uname_S),FreeBSD)
	PLATFORM = freebsd
endif
ifeq ($(uname_V6),joyent)
	PLATFORM = smartos
endif

include fifo.mk

# the kstat library will not compile on OS X

apps/chunter/priv/zonedoor: utils/zonedoor.c
# Only copile the zonedoor under sunus
	[ $(shell uname) != "SunOS" ] && cp `which cat` apps/chunter/priv/zonedoor || gcc -lzdoor utils/zonedoor.c -o apps/chunter/priv/zonedoor

version_header: apps/chunter/priv/zonedoor
	@./version_header.sh
	@cp chunter.version rel/files/chunter.version
