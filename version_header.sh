#!/bin/sh

case $(uname) in
  SunOS)
    gcc -lzdoor utils/zonedoor.c -o apps/chunter/priv/zonedoor
    ;;
  *)
    cp `which cat` apps/chunter/priv/zonedoor
   ;;
esac
echo "$(git symbolic-ref HEAD 2> /dev/null | cut -b 12-)-$(git log --pretty=format:'%h, %ad' -1)" > chunter.version
echo "-define(VERSION, <<\"$(cat chunter.version)\">>)." > apps/chunter/src/chunter_version.hrl
