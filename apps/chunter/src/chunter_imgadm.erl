-module(chunter_imgadm).

-export([import/1, get/1]).


get(Image) ->
    fifo_cmd:run_json(imgadm_cmd(), ["get", Image]).

import(Image) ->
    case fifo_cmd:run(imgadm_cmd(), ["import", q, Image]) of
        {ok, <<"Importing ", UUID:36/binary, _/binary>>} ->
            {ok, UUID};
        {ok, <<"Image ", UUID:36/binary, _/binary>>} ->
            {ok, UUID};
        {error, 1, <<"Image ", UUID:36/binary, _/binary>>} ->
            {ok, UUID};
        Error ->
            lager:error("[imgadm] Could not import image ~s: ~p",
                        [Image, Error]),
            Error
    end.

imgadm_cmd() ->
    case chunter_utils:system() of
        freebsd ->
            "/usr/local/sbin/imgadm";
        smartos ->
            "/usr/sbin/imgadm"
    end.
