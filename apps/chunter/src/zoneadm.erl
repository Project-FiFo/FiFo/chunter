-module(zoneadm).
-behaviour(vmctl).
-define(ZONEADM, "/usr/sbin/zoneadm").

-export([list/0,
         start/1,
         start/2,
         delete/1,
         info/1,
         stop/1,
         force_stop/1,
         reboot/1,
         force_reboot/1,
         update/2,
         create/2,
         load/1
        ]).

-export([zonecfg/1]).
%%%===================================================================
%%% API
%%%===================================================================

list() ->
    [#{uuid => UUID, name => UUID, state => VMState} ||
        %% SmartOS seems to have one more coumn
        [ID, UUID, VMState, _Path, _OtherUUID, _Type, _IP | _] <-
            zoneadm_list(), ID =/= <<"0">>].

-spec start(UUID::fifo:uuid()) -> list().
start(UUID) ->
    lager:info("zoneadm:start - UUID: ~s.", [UUID]),
    zoneadm(UUID, boot).

-spec start(UUID::fifo:uuid(), Image::binary()) -> list().
start(UUID, Image) ->
    lager:info("zoneadm:start - UUID: ~s, Image: ~s.", [UUID, Image]),
    zoneadm(UUID, boot).

-spec delete(UUID::fifo:uuid()) -> string().
delete(UUID) ->
    {ok, VM} =  load(UUID),
    force_stop(UUID),
    lager:info("zoneadm:uninstall - UUID: ~s / ~p.", [UUID, VM]),
    zoneadm(UUID, uninstall),
    zonecfg(UUID, delete),
    {ok, Nics} = jsxd:get(<<"nics">>, VM),
    lists:map(fun(N) ->
                      {ok, IFace} = jsxd:get(<<"interface">>, N),
                      chunter_nic_srv:delete(IFace),
                      $. %% This is so we have a nice list of dots
              end, Nics).

-spec info(UUID::fifo:uuid()) -> {ok, fifo:config_list()} | {error, no_info}.
info(_UUID) ->
    {error, no_info}.

-spec stop(UUID::fifo:uuid()) -> list().
stop(UUID) ->
    lager:info("zoneadm:stop - UUID: ~s.", [UUID]),
    zoneadm(UUID, shutdown).

-spec force_stop(UUID::fifo:uuid()) -> list().

force_stop(UUID) ->
    lager:info("vmadm:force-stop - UUID: ~s.", [UUID]),
    zoneadm(UUID, halt).

-spec reboot(UUID::fifo:uuid()) -> list().

reboot(UUID) ->
    lager:info("vmadm:reboot - UUID: ~s.", [UUID]),
    zoneadm(UUID, "shutdown -r").

-spec force_reboot(UUID::fifo:uuid()) -> list().

force_reboot(UUID) ->
    lager:info("vmadm:reboot - UUID: ~s.", [UUID]),
    zoneadm(UUID, reboot).

update(_UUID, _Data) ->
    {error, <<"not supported">>}.

create(_UUID, _Data) ->
    {error, <<"not supported">>}.

load(UUID) when is_binary(UUID)  ->
    load(#{name => UUID, uuid => UUID});

load(VM) when is_map(VM) ->
    load_zone(VM).

zonecfg(L) ->
    [zonecfg1(C) || C <- L].

%%%===================================================================
%%% Internal functions
%%%===================================================================

zoneadm_list() ->
    [re:split(Line, ":")
     || Line <- re:split(os:cmd("/usr/sbin/zoneadm list -ip"), "\n")].

zoneadm(UUID, uninstall) ->
    zoneadmf(UUID, <<"uninstall">>);
zoneadm(UUID, SubCmd) when is_atom(SubCmd) ->
    zoneadm(UUID, atom_to_list(SubCmd));
zoneadm(UUID, SubCmd) when is_list(SubCmd) ->
    zoneadm(UUID, list_to_binary(SubCmd));

zoneadm(UUID, SubCmd) ->
    Cmd = <<"/usr/sbin/zoneadm -z ", UUID/binary, " ", SubCmd/binary>>,
    lager:debug("zoneadm:cmd - ~s.", [Cmd]),
    R = os:cmd(binary_to_list(Cmd)),
    lager:debug("[zoneadm] ~s", [R]),
    R.

zoneadmf(UUID, SubCmd) ->
    Cmd = <<"/usr/sbin/zoneadm -z ", UUID/binary, " ", SubCmd/binary, " -F">>,
    lager:debug("zoneadm:cmd - ~s.", [Cmd]),
    R = os:cmd(binary_to_list(Cmd)),
    lager:debug("[zoneadm] ~s", [R]),
    R.

zonecfg(UUID, SubCmd) when is_atom(SubCmd) ->
    zonecfg(UUID, atom_to_list(SubCmd));
zonecfg(UUID, SubCmd) when is_list(SubCmd) ->
    zonecfg(UUID, list_to_binary(SubCmd));

zonecfg(UUID, SubCmd) ->
    Cmd = <<"/usr/sbin/zonecfg -z ", UUID/binary, " ", SubCmd/binary>>,
    lager:debug("zonecfg:cmd - ~s.", [Cmd]),
    R = os:cmd(binary_to_list(Cmd)),
    lager:debug("[zonecfg] ~s", [R]),
    R.



-type xml_element() :: {string(), [{string(), term()}], [xml_element()]}.

-define(REMOVE(Key),
        create_zone_data([{Key, _}|R], Disks, Nics, Datasets) ->
               create_zone_data(R, Disks, Nics, Datasets)).

-define(RENAME_INT(Old, New),
        create_zone_data([{Old, Value}|R], Disks, Nics, Datasets) ->
               {Num, []} = string:to_integer(binary_to_list(Value)),
               [{New, Num}
                |create_zone_data(R, Disks, Nics, Datasets)]).

-define(RENAME_INT_B2MB(Old, New),
        create_zone_data([{Old, Value}|R], Disks, Nics, Datasets) ->
               {Num, []} = string:to_integer(binary_to_list(Value)),
               [{New, Num div (1024 * 1024) }
                | create_zone_data(R, Disks, Nics, Datasets)]).

-define(RENAME_B64(Old, New),
        create_zone_data([{Old, Value}|R], Disks, Nics, Datasets) ->
               [{New, base64:decode(Value)}
                |create_zone_data(R, Disks, Nics, Datasets)]).

-define(RENAME_BOOL(Old, New),
        create_zone_data([{Old, <<"true">>}|R], Disks, Nics, Datasets) ->
               [{New, true}|create_zone_data(R, Disks, Nics, Datasets)];
            create_zone_data([{Old, <<"false">>}|R], Disks, Nics, Datasets) ->
               [{New, false}|create_zone_data(R, Disks, Nics, Datasets)]).

-define(RENAME_SPLIT(Old, New),
        create_zone_data([{Old, Value}|R], Disks, Nics, Datasets) ->
               [{New, re:split(Value, ", ")}
                |create_zone_data(R, Disks, Nics, Datasets)]).

-define(RENAME(Old, New),
        create_zone_data([{Old, Value}|R], Disks, Nics, Datasets) ->
               [{New, Value}
                |create_zone_data(R, Disks, Nics, Datasets)]).

-define(NIC_RENAME(Old, New),
        create_nic([{Old, Value}|R]) ->
               [{New, Value}
                |create_nic(R)]).
-define(NIC_RENAME_INT(Old, New),
        create_nic([{Old, Value}|R]) ->
               {Num, []} = string:to_integer(binary_to_list(Value)),
               [{New, Num}
                |create_nic(R)]).
-define(NIC_RENAME_BOOL(Old, New),
        create_nic([{Old, <<"true">>}|R]) ->
               [{New, true}
                |create_nic(R)];
            create_nic([{Old, <<"false">>}|R]) ->
               [{New, false}
                |create_nic(R)]).

-define(DISK_RENAME(Old, New),
        create_disk([{Old, Value}|R]) ->
               [{New, Value}
                |create_disk(R)]).

-define(DISK_RENAME_INT(Old, New),
        create_disk([{Old, Value}|R]) ->
               {Num, []} = string:to_integer(binary_to_list(Value)),
               [{New, Num}
                |create_disk(R)]).
-define(DISK_RENAME_BOOL(Old, New),
        create_disk([{Old, <<"true">>}|R]) ->
               [{New, true}
                |create_disk(R)];
            create_disk([{Old, <<"false">>}|R]) ->
               [{New, false}
                |create_disk(R)]).

%% The way vmadm handles maintain_resolvers is different to other boolean fields
%% so we got to adapt for that and actively set it false when it's not true.
apply_resolvers(ZUUID, VM) ->
    VM1 = jsxd:update([<<"maintain_resolvers">>], fun(V) -> V end, false, VM),
    apply_indestructible(ZUUID, VM1).

apply_indestructible(ZUUID, VM) ->
    VM#{
      <<"indestructible_zoneroot">> => indestructible_zoneroot(ZUUID),
      <<"indestructible_delegated">> => indestructible_delegated(ZUUID)
     }.


indestructible(D) ->
    case chunter_zfs:list(<<D/binary, "@indestructible">>) of
        {ok, _} ->
            true;
        _ ->
            false
    end.
indestructible_zoneroot(UUID) ->
    indestructible(chunter_zfs:dataset(UUID)).
indestructible_delegated(UUID) ->
    indestructible(chunter_zfs:dataset(<<UUID/binary, "/data">>)).

load_zone(#{name := Name} = VM) ->
    case convert(<<"/etc/zones/", Name/binary, ".xml">>,
                 maps:to_list(VM)) of
        {ok, Res} ->
            Dataset = chunter_zfs:dataset(Name),
            RouteFile = <<"/", Dataset/binary, "/config/routes.json">>,
            Res1 = case filelib:is_file(RouteFile) of
                       false ->
                           Res;
                       true ->
                           {ok, Routes} = file:read_file(RouteFile),
                           RoutesJSON = jsone:decode(Routes),
                           jsxd:set(<<"routes">>, RoutesJSON, Res)
                   end,
            Res2 = apply_resolvers(Name, Res1),
            {ok, apply_indestructible(Name, Res2)};
        E ->
            E
    end.

-spec convert(Name::binary(), VM::[{binary(), term()}]) ->
                     {ok, fifo:vm_config()} |
                     {error, not_found} |
                     {error, atom()}.

convert(F, VM)->
    case file:read_file(F) of
        {ok, XML}  ->
            case erlsom:simple_form(XML) of
                {ok, {"zone", Attrs, Value}, _}->
                    {ok, jsxd:from_list(create_zone_data(
                                          VM ++ map_attrs(Attrs)
                                          ++ parse_xml(Value)))};
                Err->
                    Err
            end;
        _ ->
            {error, not_found}
    end.

-spec parse_xml([xml_element()]) ->
                       [xml_element() | {Key::binary()|atom(), Value::term()}].

parse_xml([{"zone", Attrib, Value}|T])->
    [{"zone", Attrib, lists:flatten(parse_xml(Value))}|parse_xml(T)];

parse_xml([{"dataset", Attrib, _Value}|T])->
    [{<<"dataset">>, list_to_binary(proplists:get_value("name", Attrib))}
     |parse_xml(T)];

parse_xml([{"attr", Attrib, _Value}|T])->
    [{list_to_binary(proplists:get_value("name", Attrib)),
      list_to_binary(proplists:get_value("value", Attrib))}
     |parse_xml(T)];

parse_xml([{"rctl", Attrib, [{"rctl-value",
                              Values,
                              _}]}|T])->
    [{list_to_binary(proplists:get_value("name", Attrib)),
      list_to_binary(proplists:get_value("limit", Values))}
     |parse_xml(T)];

parse_xml([{"net-attr", [{"name", Name}, {"value", Value}], []}|T])->
    [{list_to_binary(Name), list_to_binary(Value)}|parse_xml(T)];

parse_xml([{"net-attr", [{"value", Value}, {"name", Name}], []}|T])->
    [{list_to_binary(Name), list_to_binary(Value)}|parse_xml(T)];

parse_xml([{"device",
            [{"match",
              Path}],
            Content }|T]) ->
    [{<<"disk">>,
      [{<<"path">>, list_to_binary(Path)}|parse_xml(Content)]}
     |parse_xml(T)];

parse_xml([{"network",
            Attrs,
            Content}|T]) ->
    [{<<"nic">>,
      map_attrs(Attrs) ++ parse_xml(Content)}
     |parse_xml(T)];

parse_xml([{Node, Attribs, Value}|T])->
    [{list_to_binary(Node), map_attrs(Attribs) ++
          lists:flatten(parse_xml(Value))}|parse_xml(T)];

parse_xml(Value)->
    Value.

-spec map_attrs([{string(), string()}]) ->
                       [{binary(), binary()}].
map_attrs(Attrs) ->
    lists:map(fun ({K, V}) ->
                      {list_to_binary(K), list_to_binary(V)}
              end, Attrs).


-spec create_zone_data(Data::[{atom()|binary(), term()}]) ->
                              [{binary(), term()}].
create_zone_data(Data) ->
    create_zone_data(Data, [], [], []).

create_zone_data([], [], [], []) ->
    [];

create_zone_data([], [], [], Datasets) ->
    [{<<"datasets">>, Datasets}];

create_zone_data([], [], Nics, Datasets) ->
    [{<<"nics">>, Nics}|create_zone_data([], [], [], Datasets)];

create_zone_data([], Disks, Nics, Datasets) ->
    [{<<"disks">>, Disks}|create_zone_data([], [], Nics, Datasets)];

create_zone_data([{<<"disk">>, Disk}|R], Disks, Nics, Datasets) ->
    create_zone_data(R, [create_disk(Disk)|Disks], Nics, Datasets);

create_zone_data([{<<"nic">>, Nic}|R], Disks, Nics, Datasets) ->
    create_zone_data(R, Disks, [create_nic(Nic)|Nics], Datasets);

create_zone_data([{<<"dataset">>, Dataset}|R], Disks, Nics, Datasets) ->
    create_zone_data(R, Disks, Nics, [Dataset|Datasets]);

?RENAME(<<"create-timestamp">>, <<"created_at">>);
?RENAME(<<"owner-uuid">>, <<"owner">>);
?RENAME(<<"dns-domain">>, <<"dns_domain">>);
?REMOVE(<<"ip-type">>);
?REMOVE(<<"debugid">>);
?RENAME_B64(<<"alias">>, <<"alias">>);
?RENAME_BOOL(<<"autoboot">>, <<"autoboot">>);
?RENAME_BOOL(<<"docker">>, <<"docker">>);
?RENAME(<<"billing-id">>, <<"billing_id">>);
?RENAME_INT(<<"cpu-cap">>, <<"cpu_cap">>);
?RENAME_BOOL(<<"do-not-inventory">>, <<"do_not_inventory">>);
?RENAME(<<"name">>, <<"zonename">>);
?RENAME(<<"dataset-uuid">>, <<"dataset_uuid">>);
?RENAME_BOOL(<<"never-booted">>, <<"never_booted">>);
?RENAME_B64(<<"qemu-extra_opts">>, <<"qemu_extra_opts">>);
?RENAME_B64(<<"qemu-opts">>, <<"qemu_opts">>);
?RENAME_INT(<<"ram">>, <<"ram">>);
?RENAME_SPLIT(<<"resolvers">>, <<"resolvers">>);
?RENAME_B64(<<"spice-opts">>, <<"spice_opts">>);
?RENAME_B64(<<"spice-password">>, <<"spice_password">>);
?RENAME_INT(<<"spice-port">>, <<"spice_port">>);
?RENAME_INT(<<"tempfs">>, <<"tempfs">>);
?RENAME_INT(<<"vcpus">>, <<"vcpus">>);
?RENAME_INT(<<"virtio-txburst">>, <<"virtio_txburst">>);
?RENAME_INT(<<"virtio-txtimer">>, <<"virtio_txtimer">>);
?RENAME_BOOL(<<"vm-autoboot">>, <<"vm_autoboot">>);
?RENAME_BOOL(<<"maintain-resolvers">>, <<"maintain_resolvers">>);
?RENAME_B64(<<"vnc-password">>, <<"vnc_password">>);
?RENAME_INT(<<"vnc-port">>, <<"vnc_port">>);
?RENAME_INT(<<"zoneid">>, <<"zoneid">>);
?RENAME_INT(<<"zone.cpu-cap">>, <<"cpu_cap">>);
?RENAME_INT(<<"zone.cpu-shares">>, <<"cpu_shares">>);
?RENAME_INT_B2MB(<<"zone.max-locked-memory">>, <<"max_locked_memory">>);
?RENAME_INT(<<"zone.max-lwps">>, <<"max_lwps">>);
?RENAME_INT_B2MB(<<"zone.max-physical-memory">>, <<"max_physical_memory">>);
?RENAME_INT_B2MB(<<"zone.max-swap">>, <<"max_swap">>);
?RENAME_INT(<<"zone.zfs-io-priority">>, <<"zfs_io_priority">>);
create_zone_data([P|R], Disks, Nics, Datasets) ->
    [P|create_zone_data(R, Disks, Nics, Datasets)].

?NIC_RENAME(<<"ip">>, <<"ip">>);
?NIC_RENAME(<<"mac-addr">>, <<"mac">>);
?NIC_RENAME(<<"physical">>, <<"interface">>);
?NIC_RENAME_INT(<<"vlan-id">>, <<"vlan_id">>);
?NIC_RENAME(<<"global-nic">>, <<"nic_tag">>);
?NIC_RENAME_BOOL(<<"dhcp-server">>, <<"dhcp_server">>);

?NIC_RENAME(<<"blocked_outgoing_ports">>, <<"blocked_outgoing_ports">>);
create_nic([]) ->
    [];

create_nic([T|R]) ->
    [T|create_nic(R)].

?DISK_RENAME(<<"match">>, <<"path">>);
?DISK_RENAME_BOOL(<<"boot">>, <<"boot">>);
?DISK_RENAME(<<"image-uuid">>, <<"image_uuid">>);
?DISK_RENAME(<<"image-name">>, <<"image_name">>);

create_disk([]) ->
    [];

create_disk([{<<"image-size">>, Value}|R]) ->
    case lists:keyfind(<<"size">>, 1, R) of
        false ->
            {Num, []} = string:to_integer(binary_to_list(Value)),
            [{<<"size">>, Num}
             |create_disk(R)];
        _ ->
            create_disk(R)
    end;

create_disk([{<<"size">>, Value}|R]) ->
    {Num, []} = string:to_integer(binary_to_list(Value)),
    [{<<"size">>, Num}
     |lists:keydelete(<<"size">>, 1, create_disk(R))];

create_disk([T|R]) ->
    [T|create_disk(R)].

%% todo:
%% <<"filesystem">>, {
%%     <<"special">>, <<"source">>,
%%     <<"directory">>, <<"target">>,
%%     <<"type">>, <<"type">>,
%%     <<"raw">>, <<"raw">>
%% },

zonecfg1(create) ->
    "create\n";

zonecfg1(verify) ->
    "verify\n";

zonecfg1(commit) ->
    "commit\n";

zonecfg1(exit) ->
    "exit\n";


zonecfg1({N, V}) when is_atom(N) ->
    set(N, V);


zonecfg1({rctl, Name, Priv, Limit, Action}) ->
    ["add rctl\n",
     "set name=", v(Name), $\n,
     "add value ", rctl_val(Priv, Limit, Action), $\n,
     "end\n"];

zonecfg1({property, Name, Value}) ->
    ["add property ",
     val_list([{name, Name}, {value, ["\"" , Value, "\""]}]),
     $\n];

zonecfg1({add, What, Opts}) ->
    add(What, Opts);

zonecfg1({attr, Name, Type, Value}) ->
    add(attr,
        [{name, Name},
         {type, Type},
         {value, Value}]).

add(Type, Values) ->
    ["add ", v(Type), $\n,
     [zonecfg1(Value) || Value <- Values],
     "end\n"].

set(Name, Value) ->
    ["set ", v(Name), "=", v(Value), $\n].

rctl_val(Priv, Limit, Action) ->
    val_list([{priv, Priv}, {limit, Limit}, {action, rctl_action(Action)}]).

val_list(Vs) ->
    [$(, string:join([[v(N), $=, v(V)] || {N, V} <- Vs], ","),  $)].

rctl_action(none) ->
    "none";
rctl_action(deny) ->
    "deny".

v(I) when is_integer(I) ->
    integer_to_list(I);

v(B) when is_binary(B) ->
    B;

v({list, L}) ->
    string:join([v(E) || E <- L], ",");

v([_E | _] = L) when not is_integer(_E) ->
    v({list, L});

v(L) when is_list(L) ->
    L;
v(A) when is_atom(A) ->
    atom_to_list(A).
