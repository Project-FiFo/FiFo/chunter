-module(vmctl).
-export([list/0,
         list_/0,
         start/1,
         start/2,
         delete/1,
         info/1,
         stop/1,
         force_stop/1,
         reboot/1,
         force_reboot/1,
         create/2,
         update/2,
         load/1
        ]).
-export_types([vmlist/0]).

-ignore_xref([behaviour_info/1]).

-type vmbase() :: #{
              uuid  => binary(),
              name  => binary(),
              state => binary()
             }.
-type vmlist() :: [vmbase()].

-callback list() -> vmlist().
-callback start(fifo:uuid()) -> list().
-callback start(fifo:uuid(), binary()) -> list().
-callback delete(fifo:uuid()) -> list().
-callback info(fifo:uuid()) -> {ok, fifo:config_list()} | {error, no_info}.
-callback stop(fifo:uuid()) -> list().
-callback force_stop(fifo:uuid()) -> list().
-callback reboot(fifo:uuid()) -> list().
-callback force_reboot(fifo:uuid()) -> list().
-callback create(UUID::binary(), Data::fifo:vm_config()) ->
    ok | {error, binary() | timeout | unknown}.
-callback update(UUID::binary(), Data::fifo:vm_config()) ->
    ok | {error, binary() | timeout | unknown}.
-callback load(fifo:uuid() | vmbase()) ->
    {ok, fifo:config_list()} | {error, not_found}.


%%%===================================================================
%%% API
%%%===================================================================

-spec list() -> [fifo:config_list()].
list() ->
    %% TODO: find a way to unify this!
    L1 = [load(R) || R <- list_()],
    [E || {ok, E} <- L1].

-spec list_() -> vmlist().
list_() ->
    CLI = cli(),
    CLI:list().

-spec load(fifo:uuid() | vmbase()) ->
                  {ok, fifo:config_list()} | {error, not_found}.
load(VM) ->
    CLI = cli(),
    CLI:load(VM).


-spec start(UUID::fifo:uuid()) -> list().
start(UUID) ->
    CLI = cli(),
    CLI:start(UUID).

-spec start(UUID::fifo:uuid(), binary()) -> list().
start(UUID,  Image) ->
    CLI = cli(),
    CLI:start(UUID,  Image).

-spec delete(UUID::fifo:uuid()) -> string().
delete(UUID) ->
    CLI = cli(),
    R1 = CLI:delete(UUID),
    chunter_server:update_mem(),
    R1.

-spec info(UUID::fifo:uuid()) -> {ok, fifo:config_list()} | {error, no_info}.
info(UUID) ->
    CLI = cli(),
    CLI:info(UUID).

-spec stop(UUID::fifo:uuid()) -> list().
stop(UUID) ->
    CLI = cli(),
    CLI:stop(UUID).

-spec force_stop(UUID::fifo:uuid()) -> list().
force_stop(UUID) ->
    CLI = cli(),
    CLI:stop(UUID).

-spec reboot(UUID::fifo:uuid()) -> list().
reboot(UUID) ->
    CLI = cli(),
    CLI:reboot(UUID).

-spec force_reboot(UUID::fifo:uuid()) -> list().
force_reboot(UUID) ->
    CLI = cli(),
    CLI:reboot(UUID).

-spec create(UUID::binary(), Data::fifo:vm_config()) -> ok |
                                                        {error, binary() |
                                                         timeout |
                                                         unknown}.
create(UUID, Data) ->
    CLI = cli(),
    CLI:create(UUID,  Data).

-spec update(UUID::binary(), Data::fifo:vm_config()) -> ok |
                                                        {error, binary() |
                                                         timeout |
                                                         unknown}.
update(UUID, Data) ->
    CLI = cli(),
    CLI:update(UUID,  Data).

%%%===================================================================
%%% Internal functions
%%%===================================================================
cli() ->
    chunter_utils:vm_cli().
