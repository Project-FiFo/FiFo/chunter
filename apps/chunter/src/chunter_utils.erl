%%%-------------------------------------------------------------------
%%% @author Heinz Nikolaus Gies <heinz@licenser.net>
%%% @copyright (C) 2015, Heinz Nikolaus Gies
%%% @doc
%%%
%%% @end
%%% Created : 25 May 2015 by Heinz Nikolaus Gies <heinz@licenser.net>
%%%-------------------------------------------------------------------
-module(chunter_utils).

-export([has_feature/1, sysinfo/0, system/0, vm_cli/0, arch/0]).


-type vm_cli() :: zoneadm | vmadm.
-type feature() :: zlogin | zdoor | sysinfo | zonemon | smf | vm_cli().
-type system() :: smartos | omnios | solaris | freebsd.




sysinfo() ->
    case has_feature(sysinfo) of
        true ->
            BinResponse = list_to_binary(os:cmd("sysinfo")),
            SysInfo0 = jsone:decode(BinResponse),
            SysInfo = jsxd:delete([<<"Boot Parameters">>, <<"root_shadow">>],
                                  SysInfo0),
            {ok, SysInfo};
        false ->
            case system() of
                freebsd ->
                    {ok, #{<<"Boot Parameters">> => #{<<"freebsd">> => true}}};
                _ ->
                    {ok, []}
            end
    end.

-spec system() -> system().

system() ->
    case application:get_env(chunter, system) of
        undefined ->
            S = system_(),
            application:set_env(chunter, system, S),
            S;
        {ok, S} ->
            S
    end.

-spec system_() -> system().

system_() ->
    case os:cmd("uname -v") of
        "joyent" ++ _ ->
            smartos;
        "omnios" ++ _ ->
            omnios;
        "11." ++ _ ->
            solaris;
        "FreeBSD " ++ _ ->
            freebsd;
        _ ->
            undefined
    end.

-spec arch() -> ft_hypervisor:arch() | undefined.
arch() ->
    case os:cmd("uname -m") of
        "x86_64" ++ _ ->
            x86;
        "amd64"  ++ _ ->
            x86;
        "arm64" ++ _ ->
            arm64;
        _ ->
            undefined
    end.

-spec has_feature(feature()) -> boolean().

has_feature(Feature) ->
    has_feature(Feature, system()).


-spec vm_cli() -> vm_cli().
vm_cli() ->
     case {has_feature(vmadm),
           has_feature(zoneadm)} of
         {true, _} ->
             vmadm;
         {_, true} ->
             zoneadm
     end.

-spec has_feature(feature(), system()) -> boolean().

has_feature(sysinfo, smartos) ->
    true;

has_feature(zlogin, smartos) ->
    true;
has_feature(zlogin, omnios) ->
    true;
has_feature(zlogin, solaris) ->
    true;
has_feature(zlogin, freebsd) ->
    true;

has_feature(zonemon, smartos) ->
    true;
has_feature(zonemon, omnios) ->
    true;
has_feature(zonemon, solaris) ->
    true;

has_feature(smf, smartos) ->
    true;
has_feature(smf, omnios) ->
    true;
has_feature(smf, solaris) ->
    true;

has_feature(zdoor, smartos) ->
    true;
has_feature(zdoor, omnios) ->
    true;
has_feature(zdoor, solaris) ->
    true;

has_feature(vmadm, smartos) ->
    true;
has_feature(vmadm, freebsd) ->
    true;
has_feature(zoneadm, omnios) ->
    true;
has_feature(zoneadm, solaris) ->
    true;

has_feature(_, _) ->
    false.
